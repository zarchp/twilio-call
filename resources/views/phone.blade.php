@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Click To Call</div>
                <div class="card-body">
                    <p>
                        Click To Call converts your website's users into engaged customers by
                        creating an easy way for your customers to contact your sales and
                        support teams right on your website.
                    </p>
                    <p>Here's an example of how it's done!</p>
                    <hr>
                    <form action="" method="GET" id="contactForm" role="form">
                        <div class="form-group">
                            <h3>Call Sales</h3>
                            <p class="help-block">
                                Are you interested in impressing your friends and
                                confounding your enemies? Enter your phone number
                                below, and our team will contact you right away.
                            </p>
                        </div>
                        <label>Your number</label>
                        <div class="form-group">
                           <input class="form-control" type="text" name="userPhone" id="userPhone"
                                  placeholder="(651) 555-7889">
                        </div>
                        <label>Sales team number</label>
                        <div class="form-group">
                           <input class="form-control" type="text" name="salesPhone" id="salesPhone"
                                  placeholder="(651) 555-7889">
                         </div>
                        <button type="submit" class="btn btn-primary">
                            Contact Sales
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(function() {
            // Initialize phone number text input plugin
            $('#userPhone, #salesPhone').intlTelInput({
                responsiveDropdown: true,
                autoFormat: true,
                utilsScript: '/vendor/intl-phone/libphonenumber/build/utils.js',
                preferredCountries: ["id"],
                initialCountry: "id"
            });

            // Intercept form submission and submit the form with ajax
            $('#contactForm').on('submit', function(e) {
                // Prevent submit event from bubbling and automatically submitting the
                // form
                e.preventDefault();

                // Call our ajax endpoint on the server to initialize the phone call
                $.ajax({
                    url: '/call',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        userPhone: $('#userPhone').val(),
                        salesPhone: $('#salesPhone').val()
                    }
                }).done(function(data) {
                    // The JSON sent back from the server will contain a success message
                    console.log(data.message);
                }).fail(function(error) {
                    console.log(JSON.stringify(error));
                });
            });
        });
    </script>
@endsection
